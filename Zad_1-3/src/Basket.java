public class Basket {
    public static String items = "";
    public static int totalPrice = 0;
    public static double totalWeight = 10;

    public static void main(String[] args) {
        add("Колбаса", 80, 5);
        add("Колбаса", 80, 5);
        add("Хлеб", 40, 0.6);
        add("Молоко", 100, 2.5);
        print("Содержимое корзины равно:");
        int totalPrice = getTotalPrice();
        System.out.println("Общая стоимость товаров:" + totalPrice);
        double totalWeight = getTotalWeight();
        System.out.println("Общая масса товаров: " + totalWeight);
        clear();
        System.out.println();
        print("Содержимое корзины: ");
        totalPrice = getTotalPrice();
        System.out.println("Общая стоимость товаров:" + totalPrice);
        totalWeight = getTotalWeight();
        System.out.println("Общая масса товаров: " + totalWeight);
    }

    public static void add(String name, int price, double weight) {
        if (contains(name)) {
            return;
        }
        items = items + "\n" + name + " - " + price;
        totalPrice = totalPrice + price;
        totalWeight = totalWeight + weight;
    }

    public static void clear() {
        items = "";
        totalPrice = 0;
        totalWeight = 0;
    }

    public static int getTotalPrice() {
        return totalPrice;
    }

    public static double getTotalWeight() {
        return totalWeight;
    }

    public static boolean contains(String name) {
        return items.contains(name);
    }


    public static void print(String title) {
        System.out.println(title);
        if (items.isEmpty()) {
            System.out.println("Корзина пуста!");
        } else {
            System.out.println(items);
        }

    }

}
