public class Printer {
    private String queue = "";
    private int PendingPagesCount = 0;


    public void append(String document_text, String document_name, int number_of_pages) {
        queue = queue + "Текст всего документа: " + document_text + ";" + " " + "Имя документа: " + document_name + ";" + " " + "Количество страниц документа: " + number_of_pages + "\n" + "Вывод листов документов: ";
        PendingPagesCount = PendingPagesCount + number_of_pages;
    }

    public void append(String document_text, String document_name) {
        append(document_text, document_name, 0);
    }

    public void append(String document_text) {
        append(document_text, "", 0);
    }

    public int getPendingPagesCount() {
        return PendingPagesCount;
    }

    public void clear() {
        queue = "";
        PendingPagesCount = 0;
    }

    public void print(String title) {
        System.out.println(title);
        System.out.println(queue + PendingPagesCount);
    }

}
