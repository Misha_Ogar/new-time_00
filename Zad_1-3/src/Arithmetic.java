public class Arithmetic {
    private int a = 0;
    private int b = 0;
    private int sum = 0;
    private int work = 0;
    private int max = 0;
    private int min = 0;

    public Arithmetic(int variable1, int variable2) {
        this.a = variable1;
        this.b = variable2;
    }

    public void add(int variable3, int variable4) {
        this.sum = this.sum + variable3 + variable4;
        this.work += variable3 * variable4;
        if (variable3 >= variable4) {
            this.max = variable3;
        } else {
            this.max = variable4;
        }

        if (variable3 <= variable4) {
            this.min = variable3;
        } else {
            this.min = variable4;
        }

    }

    public void print(String title) {
        System.out.println(title);
        System.out.println("Сумма чисел равна: " + this.sum + "\n" + "Произведение чисел равно: " + this.work + "\n" + "Максимальное число: " + this.max + "\n" + "Минимальное число: " + this.min);
    }

}
